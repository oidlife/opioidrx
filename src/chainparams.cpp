// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2014 The Bitcoin Core developers
// Copyright (c) 2014-2017 The OpioidRX Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
// OpioidRX core v12.3.0 cloned from Sparks Reborn

#include "chainparams.h"
#include "consensus/merkle.h"

#include "tinyformat.h"
#include "util.h"
#include "utilstrencodings.h"

#include <assert.h>

#include <boost/assign/list_of.hpp>

#include "chainparamsseeds.h"

static CBlock CreateGenesisBlock(const char* pszTimestamp, const CScript& genesisOutputScript, uint32_t nTime, uint32_t nNonce, uint32_t nBits, int32_t nVersion, const CAmount& genesisReward)
{
    CMutableTransaction txNew;
    txNew.nVersion = 1;
    txNew.vin.resize(1);
    txNew.vout.resize(1);
    txNew.vin[0].scriptSig = CScript() << 486604799 << CScriptNum(4) << std::vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));
    txNew.vout[0].nValue = genesisReward;
    txNew.vout[0].scriptPubKey = genesisOutputScript;

    CBlock genesis;
    genesis.nTime    = nTime;
    genesis.nBits    = nBits;
    genesis.nNonce   = nNonce;
    genesis.nVersion = nVersion;
    genesis.vtx.push_back(txNew);
    genesis.hashPrevBlock.SetNull();
    genesis.hashMerkleRoot = BlockMerkleRoot(genesis);
    return genesis;
}

/**
 * Build the genesis block. Note that the output of its generation
 * transaction cannot be spent since it did not originally exist in the
 * database.
 *
 * CBlock(hash=00000ffd590b14, ver=1, hashPrevBlock=00000000000000, hashMerkleRoot=e0028e, nTime=1390095618, nBits=1e0ffff0, nNonce=28917698, vtx=1)
 *   CTransaction(hash=e0028e, ver=1, vin.size=1, vout.size=1, nLockTime=0)
 *     CTxIn(COutPoint(000000, -1), coinbase 04ffff001d01044c5957697265642030392f4a616e2f3230313420546865204772616e64204578706572696d656e7420476f6573204c6976653a204f76657273746f636b2e636f6d204973204e6f7720416363657074696e6720426974636f696e73)
 *     CTxOut(nValue=50.00000000, scriptPubKey=0xA9037BAC7050C479B121CF)
 *   vMerkleTree: e0028e
 */
static CBlock CreateGenesisBlock(uint32_t nTime, uint32_t nNonce, uint32_t nBits, int32_t nVersion, const CAmount& genesisReward)
{
    //message change!!
    const char* pszTimestamp = "The OpioidRX Genesis 07.29.2018: Ok, let's cure people!";
    //change hex value!! 
    const CScript genesisOutputScript = CScript() << ParseHex("04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f") << OP_CHECKSIG;
    //04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f
    //04b8898d0426085544ef12d689123d397af458983a4d38fb325e15781be66870c84f75e3c9d202506731ec7dbd806d8f2b9b58ff987c286b957b3557b0a9d36335
    //04ffff001d010436546865204f70696f696452582047656e657369732030372e31372e323031383a204f6b2c206c65747320637572652070656f706c6521


    //go to genesis-block on github
    return CreateGenesisBlock(pszTimestamp, genesisOutputScript, nTime, nNonce, nBits, nVersion, genesisReward);
}

/**
 * Main network
 */
/**
 * What makes a good checkpoint block?
 * + Is surrounded by blocks with reasonable timestamps
 *   (no blocks before with a timestamp after, none after with
 *    timestamp before)
 * + Contains no strange transactions
 */


class CMainParams : public CChainParams {
public:
    CMainParams() {
        strNetworkID = "main";
        consensus.nSubsidyHalvingInterval = 262800; // Note: actual number of blocks per calendar year with DGW v3 is ~200700 (for example 449750 - 249050)
        //change to 30k!!!
        consensus.nMasternodePaymentsStartBlock = 86000; // not true, but it's ok as long as it's less then nMasternodePaymentsIncreaseBlock
        consensus.nMasternodePaymentsIncreaseBlock = 158000; // actual historical value
        consensus.nMasternodePaymentsIncreasePeriod = 576*30; // 17280 - actual historical value
        consensus.nInstantSendKeepLock = 24; //send to another user without mine transaction - it's a special send using mast.node
        consensus.nBudgetPaymentsStartBlock = 2100000000; // year 10000+
        consensus.nBudgetPaymentsCycleBlocks = 16616; // ~(60*24*30)/2.6, actual number of blocks per month is 200700 / 12 = 16725
        consensus.nBudgetPaymentsWindowBlocks = 100;
        consensus.nBudgetProposalEstablishingTime = 60*60*24;
        consensus.nSuperblockStartBlock = 2100000000; // The block at which 12.1 goes live (end of final 12.0 budget cycle)
        consensus.nSuperblockCycle = 16616; // ~(60*24*30)/2.6, actual number of blocks per month is 200700 / 12 = 16725
        consensus.nGovernanceMinQuorum = 10;
        consensus.nGovernanceFilterElements = 20000;
        //change to 10
        consensus.nMasternodeMinimumConfirmations = 10;
        
        //can be changed in future as soft fork
        consensus.nMajorityEnforceBlockUpgrade = 750;
        consensus.nMajorityRejectBlockOutdated = 950;
        consensus.nMajorityWindow = 100;//was 1000
        
        consensus.BIP34Height = 227931; // FIX
        consensus.BIP34Hash = uint256S("0x000000000000024b89b42a942fe0d9fea3bb44ab7bd1b19115dd6a759c0808b8"); // FIX
        
        consensus.powLimit = uint256S("00000fffff000000000000000000000000000000000000000000000000000000");//max diff
        
        //can change these later
        consensus.nPowTargetTimespan = 60 * 60; // OpioidRX: 1 hour, 24 blocks
        consensus.nPowTargetSpacing = 2 * 60; // OpioidRX: 120 seconds
        //leave as false
        consensus.fPowAllowMinDifficultyBlocks = false;
        //leave as false
        consensus.fPowNoRetargeting = false;
        
        consensus.nPowKGWHeight = 15200; // KGW > DGW, no KGW
        //set lower aka 200
        consensus.nPowDGWHeight = 700;
        
        consensus.nRuleChangeActivationThreshold = 1916; // 95% of 2016
        consensus.nMinerConfirmationWindow = 2016; // nPowTargetTimespan / nPowTargetSpacing
        
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].bit = 28;
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].nStartTime = 1199145601; // January 1, 2008
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].nTimeout = 1230767999; // December 31, 2008

        // Deployment of BIP68, BIP112, and BIP113.
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].bit = 0;
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].nStartTime = 1502280000; // Aug 9th, 2017
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].nTimeout = 1533816000; // Aug 9th, 2018

        // Deployment of DIP0001
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].bit = 1;
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nStartTime = 1528502400; // June 9th, 2018
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nTimeout = 1560556800; // June 15th, 2019
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nWindowSize = 1000;
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nThreshold = 600;

        // The best chain should have at least this much work.
        // put at x0 to start chain
        consensus.nMinimumChainWork = uint256S("0x");//117400
        //was 0x00000000000000000000000000000000000000000000000000dde3dfa3ad67fd

        // By default assume that the signatures in ancestors of this block are valid.
        // put at x0 to start chain
        consensus.defaultAssumeValid = uint256S("0x"); //117400
        //was 0x000000000596de322263352224b2b9f5030fe63e003433791ecec5676755923b

        //OpioidRX stuff
        consensus.nOIDHeight = 100000;
        consensus.nOIDPremine = 650000;// number of supply for coin to coin swaps etc. 
        //consensus.nSPKPostmine = 300000;
        consensus.nOIDSubsidyLegacy = 500;
        //consensus.nSPKSubidyReborn = 20;
        consensus.nOIDBlocksPerMonth = 21600;
        //change this when get new blockchain running
        consensus.strCoreAddress = "";//<--put premine wallet here
        consensus.fOIDRatioMN = 0.7;
        //ban bad people 
        consensus.vBannedAddresses.push_back("Dx");
        consensus.vBannedAddresses.push_back("Dx");
        consensus.vBannedAddresses.push_back("Dx");

        /**
         * The message start string is designed to be unlikely to occur in normal data.
         * The characters are rarely used upper ASCII, not valid as UTF-8, and produce
         * a large 32-bit integer with any alignment.
         */
        //leave this alone
        pchMessageStart[0] = 0x2b;// was 1a
        pchMessageStart[1] = 0xc3;// was b2
        pchMessageStart[2] = 0xd4;// was c3
        pchMessageStart[3] = 0xe5;// was d4
        
        vAlertPubKey = ParseHex("0x");//was 0343
        //1234783664fe0a0f884ceff5fe128e674d8c050cfd5bd66d262a7d4e1a546e30c3
        
        nDefaultPort = 7777;//8890
        //leave alone
        nMaxTipAge = 6 * 60 * 60; // ~144 blocks behind -> 2 x fork detection time, was 24 * 60 * 60 in bitcoin
        nDelayGetHeadersTime = 24 * 60 * 60;
        nPruneAfterHeight = 100000;

        genesis = CreateGenesisBlock(1532915420, 239712, 0x1e0ffff0, 1, 50 * COIN);
        //nonce was 682979;
        consensus.hashGenesisBlock = genesis.GetHash();
        assert(consensus.hashGenesisBlock == uint256S("00000a21e4f8644a97ae10074306546286dcc4e208094f99091dd3cb59203df3"));
        //00000e3091cd3a56c6b4790fb12d4b3388fdd26c335352c511610075217f5a17
        assert(genesis.hashMerkleRoot == uint256S("16c295a9a2ad0dd8453b4f9e7ba125e95b428f586856e0957c0e700781e8c469"));
        //bc4e20fc337c37d6ba4e07c955b1aaa2140c0ff574a331dd3c6fb2ed5f65ff12
        //was 0x1b3952bab9df804c6f02372bb62df20fa2927030a4e80389ec14c1d86fc921e4

        //4 opioidRX seed nodes (same as opioid)
        vSeeds.push_back(CDNSSeedData("165.227.211.53", "165.227.211.53"));
	    //vSeeds.push_back(CDNSSeedData("oidrxseednode2.net", "oidrxseednode2.net"));
	    //vSeeds.push_back(CDNSSeedData("oidrxseednode3.net", "oidrxseednode3.net"));
	    //vSeeds.push_back(CDNSSeedData("oidrxseednode4.net", "oidrxseednode4.net"));

        // OpioidRX addresses start with 'D'
        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1,30);
        // OpioidRX script addresses start with 'X'
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1,75);
        // OpioidRX private keys start with 'X' or 'Y' (?)
        base58Prefixes[SECRET_KEY] =     std::vector<unsigned char>(1,77);
        // OpioidRX BIP32 pubkeys start with 'xpub' (Bitcoin defaults)
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x88)(0xB2)(0x1E).convert_to_container<std::vector<unsigned char> >();
        // OpioidRX BIP32 prvkeys start with 'xprv' (Bitcoin defaults)
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x88)(0xAD)(0xE4).convert_to_container<std::vector<unsigned char> >();
        // OpioidRX BIP44 coin type is '5'
        nExtCoinType = 5;

        vFixedSeeds = std::vector<SeedSpec6>(pnSeed6_main, pnSeed6_main + ARRAYLEN(pnSeed6_main));

        fMiningRequiresPeers = true;
        fDefaultConsistencyChecks = true;
        fRequireStandard = true;
        fMineBlocksOnDemand = true;
        fTestnetToBeDeprecatedFieldRPC = false;

        nPoolMaxTransactions = 3;
        nFulfilledRequestExpireTime = 60*60; // fulfilled requests expire in 1 hour

        strSporkPubKey = "0x";
        //12343174848a69d72c1ec8b59da80e912827745d8c0d8946f314203a7ade9c037e
        //was 03053174848a69d72c1ec8b59da80e912827745d8c0d8946f314203a7ade9c037e

        //change to 0,0x , look at debug.log file for transaction total number of transactions
        //00000e3091cd3a56c6b4790fb12d4b3388fdd26c335352c511610075217f5a17
        checkpointData = (CCheckpointData) {
            boost::assign::map_list_of
            (      0, uint256S("00000a21e4f8644a97ae10074306546286dcc4e208094f99091dd3cb59203df3")),
            0,//1528882815, // * UNIX timestamp of last checkpoint block
            0,//162310,    // * total number of transactions between genesis and last checkpoint
            0            //   (the tx=... number in the SetBestChain debug.log lines)
            //2800        // * estimated number of transactions per day after checkpoint
        };
    }
};
static CMainParams mainParams;

/**
 * Testnet (v3)
 */
class CTestNetParams : public CChainParams {
public:
    CTestNetParams() {
        strNetworkID = "test";
        consensus.nSubsidyHalvingInterval = 262800;
        consensus.nMasternodePaymentsStartBlock = 50; // not true, but it's ok as long as it's less then nMasternodePaymentsIncreaseBlock
        consensus.nMasternodePaymentsIncreaseBlock = 158000;
        consensus.nMasternodePaymentsIncreasePeriod = 576*30;
        consensus.nInstantSendKeepLock = 6;
        consensus.nBudgetPaymentsStartBlock = 2100000000;
        consensus.nBudgetPaymentsCycleBlocks = 50;
        consensus.nBudgetPaymentsWindowBlocks = 10;
        consensus.nBudgetProposalEstablishingTime = 60*20;
        consensus.nSuperblockStartBlock = 2100000000; // NOTE: Should satisfy nSuperblockStartBlock > nBudgetPeymentsStartBlock
        consensus.nSuperblockCycle = 24; // Superblocks can be issued hourly on testnet
        consensus.nGovernanceMinQuorum = 1;
        consensus.nGovernanceFilterElements = 500;
        consensus.nMasternodeMinimumConfirmations = 1;
        consensus.nMajorityEnforceBlockUpgrade = 51;
        consensus.nMajorityRejectBlockOutdated = 75;
        consensus.nMajorityWindow = 100;
        consensus.BIP34Height = 21111; // FIX //21111
        consensus.BIP34Hash = uint256S("0x0000000023b3a96d3484e5abb3755c413e7d41500f8e2a5c3f0dd01299cd8ef8"); // FIX
        //0x0000000000000000000000000000000000000000000000000000006726511e17
        //0x0000000000000000000000000000000000000000000000000000006726511e17
        //005514cc792c5f92c5f92c5f92c5f92c5f92c5f92c5f92c5f92c5f92c5f92c5f
        //0x0000000023b3a96d3484e5abb3755c413e7d41500f8e2a5c3f0dd01299cd8ef8

        //0x0000000023b3a96d3484e5abb3755c413e7d41500f8e2a5c3f0dd01299cd8ef8
        consensus.powLimit = uint256S("7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
        //7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
        //00000fffff000000000000000000000000000000000000000000000000000000
        //00000000ffffffffffffffffffffffffffffffffffffffffffffffffffffffff


        consensus.nPowTargetTimespan = 5 * 60; // OpioidRX: 5 minutes, 10 blocks
        consensus.nPowTargetSpacing = 0.5 * 60; // OpioidRX: 30 seconds
        consensus.fPowAllowMinDifficultyBlocks = true;
        consensus.fPowNoRetargeting = false;
        consensus.nPowKGWHeight = 4001; // nPowKGWHeight >= nPowDGWHeight means "no KGW"
        consensus.nPowDGWHeight = 650;
        consensus.nRuleChangeActivationThreshold = 1512; // 75% for testchains
        consensus.nMinerConfirmationWindow = 2016; // nPowTargetTimespan / nPowTargetSpacing
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].bit = 28;
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].nStartTime = 1199145601; // January 1, 2008
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].nTimeout = 1230767999; // December 31, 2008

        // Deployment of BIP68, BIP112, and BIP113.
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].bit = 0;
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].nStartTime = 1502280000; // Aug 9th, 2017
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].nTimeout = 1533816000; // Aug 9th, 2018

        // Deployment of DIP0001
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].bit = 1;
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nStartTime = 1528502400; // June 9th, 2018
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nTimeout = 1560556800; // June 15th, 2019
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nWindowSize = 9;
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nThreshold = 7; // 7/9 MNs

        // The best chain should have at least this much work.
        consensus.nMinimumChainWork = uint256S("0x");
        //000000000000000000000000000000000000000000000000000000d1551a0000x0
        // 2321850204b123e8d8268e5fa48a2d4a5f3ba06d6ced4c37b6682ff2ce8ae91f
        // dde3dfa3ad67fd

        // By default assume that the signatures in ancestors of this block are valid.
        consensus.defaultAssumeValid = uint256S("0x");
        //00000aaf855c482a37482ca5d19275cfda21d03548d55649c446c01b873d3770

        //OpioidRX stuff
        consensus.nOIDHeight = 100000;
        consensus.nOIDPremine = 800000;//test payout
        //consensus.nSPKPostmine = 300000;
        consensus.nOIDSubsidyLegacy = 500;// is this block reward (this is split from masternodes and miners)
        //consensus.nSPKSubidyReborn = 500; //is this block reward
        consensus.nOIDBlocksPerMonth = 1;
        consensus.strCoreAddress = "dLrwtYFTA5i5cx2kKnbvBZJq211yanrDYu"; //test address
        consensus.fOIDRatioMN = 0.7;

        pchMessageStart[0] = 0xe2;//was d1
        pchMessageStart[1] = 0x3c;//was 2b
        pchMessageStart[2] = 0xc4;//was b3
        pchMessageStart[3] = 0x8b;//was 7a
        vAlertPubKey = ParseHex("024c9a56fedb5f6dcdfb5176c79fddc991b81731797953f962a3eb7521e960b8b6");//was 0244
        //03b52ca4ecedb86b581f960ecd45049f343d8a83bd6ba0f019ccbae23fe3d7db0e
        // was 76a9146521b2e3dfc87488e9b5896425597e4978d7c2be88ac

        //1234a0f8bd00f5497419d38623be157d80d42a83f7ed95b36387461515276b31bd
        nDefaultPort = 7778;//8891
        nMaxTipAge = 0x7fffffff; // allow mining on top of old blocks for testnet
        nDelayGetHeadersTime = 24 * 60 * 60;
        nPruneAfterHeight = 1000;

        genesis = CreateGenesisBlock(1532915420, 239712, 0x1e0ffff0, 1, 50 * COIN);
        consensus.hashGenesisBlock = genesis.GetHash();
        assert(consensus.hashGenesisBlock == uint256S("00000a21e4f8644a97ae10074306546286dcc4e208094f99091dd3cb59203df3"));
        //00000e3091cd3a56c6b4790fb12d4b3388fdd26c335352c511610075217f5a17
        //was 00000aaf855c482a37482ca5d19275cfda21d03548d55649c446c01b873d3770
        assert(genesis.hashMerkleRoot == uint256S("16c295a9a2ad0dd8453b4f9e7ba125e95b428f586856e0957c0e700781e8c469"));
        //bc4e20fc337c37d6ba4e07c955b1aaa2140c0ff574a331dd3c6fb2ed5f65ff12
        // was b39ad5eb046d139473c32667bccf4a596eb9f409b1d481709f4a68c1705c9457

        vFixedSeeds.clear();
        vSeeds.clear();
        vSeeds.push_back(CDNSSeedData("165.227.211.53",  "165.227.211.53"));

        // Testnet OpioidRX addresses start with 'd' (small d for testnet)
        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1,90);
        // Testnet OpioidRX script addresses start with '9'
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1,20);
        // Testnet private keys start with '9' or 'c' (Bitcoin defaults) (?)
        base58Prefixes[SECRET_KEY] =     std::vector<unsigned char>(1,240);
        // Testnet OpioidRX BIP32 pubkeys start with 'tpub' (Bitcoin defaults)
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x35)(0x87)(0xCF).convert_to_container<std::vector<unsigned char> >();
        // Testnet OpioidRX BIP32 prvkeys start with 'tprv' (Bitcoin defaults)
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x35)(0x83)(0x94).convert_to_container<std::vector<unsigned char> >();

        // Testnet OpioidRX BIP44 coin type is '1' (All coin's testnet default)
        nExtCoinType = 1;

        vFixedSeeds = std::vector<SeedSpec6>(pnSeed6_test, pnSeed6_test + ARRAYLEN(pnSeed6_test));

        fMiningRequiresPeers = true;
        fDefaultConsistencyChecks = true;
        fRequireStandard = true;
        fMineBlocksOnDemand = true;
        fTestnetToBeDeprecatedFieldRPC = false;

        nPoolMaxTransactions = 3;
        nFulfilledRequestExpireTime = 5*60; // fulfilled requests expire in 5 minutes
        strSporkPubKey = "76a91451a33a2c937134967a822d0dcf6bd2dc036794ae88ac";//was 0355
        //03b52ca4ecedb86b581f960ecd45049f343d8a83bd6ba0f019ccbae23fe3d7db0e
        //12346beefd702162910a0b765cb4f0635cf6b8754645e8e900221aa72d7ca9027f
        // was 0290bdcd89a7bbdf45b990e44558fe40ffcfdd6670bfe8225b9d6325026ef18eee
        

        //00000e3091cd3a56c6b4790fb12d4b3388fdd26c335352c511610075217f5a17 <-was 
        //00000aaf855c482a37482ca5d19275cfda21d03548d55649c446c01b873d3770 <- was 7/25
        checkpointData = (CCheckpointData) {
            boost::assign::map_list_of
            ( 0, uint256S("00000a21e4f8644a97ae10074306546286dcc4e208094f99091dd3cb59203df3")),
	0,//1513902563, // * UNIX timestamp of last checkpoint block
    0,        //0,     // * total number of transactions between genesis and last checkpoint
    0                   //   (the tx=... number in the SetBestChain debug.log lines)
            //500         // * estimated number of transactions per day after checkpoint
        };

    }
};
static CTestNetParams testNetParams;

/**
 * Regression test
 */
class CRegTestParams : public CChainParams {
public:
    CRegTestParams() {
        strNetworkID = "regtest";
        consensus.nSubsidyHalvingInterval = 150;
        consensus.nMasternodePaymentsStartBlock = 240;
        consensus.nMasternodePaymentsIncreaseBlock = 350;
        consensus.nMasternodePaymentsIncreasePeriod = 10;
        consensus.nInstantSendKeepLock = 6;
        consensus.nBudgetPaymentsStartBlock = 1000;
        consensus.nBudgetPaymentsCycleBlocks = 50;
        consensus.nBudgetPaymentsWindowBlocks = 10;
        consensus.nBudgetProposalEstablishingTime = 60*20;
        consensus.nSuperblockStartBlock = 1500;
        consensus.nSuperblockCycle = 10;
        consensus.nGovernanceMinQuorum = 1;
        consensus.nGovernanceFilterElements = 100;
        consensus.nMasternodeMinimumConfirmations = 1;
        consensus.nMajorityEnforceBlockUpgrade = 750;
        consensus.nMajorityRejectBlockOutdated = 950;
        consensus.nMajorityWindow = 1000;
        consensus.BIP34Height = -1; // BIP34 has not necessarily activated on regtest
        consensus.BIP34Hash = uint256();
        consensus.powLimit = uint256S("7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
        consensus.nPowTargetTimespan = 60 * 60; // OpioidRX: 1 hour, 24 blocks
        consensus.nPowTargetSpacing = 2 * 60; // OpioidRX: 150 seconds
        consensus.fPowAllowMinDifficultyBlocks = true;
        consensus.fPowNoRetargeting = true;
        consensus.nPowKGWHeight = 15200; // same as mainnet
        consensus.nPowDGWHeight = 700; // same as mainnet
        consensus.nRuleChangeActivationThreshold = 108; // 75% for testchains
        consensus.nMinerConfirmationWindow = 144; // Faster than normal for regtest (144 instead of 2016)
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].bit = 28;
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].nStartTime = 0;
        consensus.vDeployments[Consensus::DEPLOYMENT_TESTDUMMY].nTimeout = 999999999999ULL;
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].bit = 0;
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].nStartTime = 0;
        consensus.vDeployments[Consensus::DEPLOYMENT_CSV].nTimeout = 999999999999ULL;
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].bit = 1;
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nStartTime = 0;
        consensus.vDeployments[Consensus::DEPLOYMENT_DIP0001].nTimeout = 999999999999ULL;

        // The best chain should have at least this much work.
        consensus.nMinimumChainWork = uint256S("0x");

        // By default assume that the signatures in ancestors of this block are valid.
        consensus.defaultAssumeValid = uint256S("0x");

        //OpioidRX stuff
        consensus.nOIDHeight = 100;
        consensus.nOIDPremine = 650000;
        //consensus.nSPKPostmine = 300000;
        consensus.nOIDSubsidyLegacy = 18;
        //consensus.nSPKSubidyReborn = 20;
        consensus.nOIDBlocksPerMonth = 1;
        consensus.strCoreAddress = "";

        pchMessageStart[0] = 0xa1;
        pchMessageStart[1] = 0xb3;
        pchMessageStart[2] = 0xd5;
        pchMessageStart[3] = 0x7b;
        nMaxTipAge = 6 * 60 * 60; // ~144 blocks behind -> 2 x fork detection time, was 24 * 60 * 60 in bitcoin
        nDelayGetHeadersTime = 0; // never delay GETHEADERS in regtests
        nDefaultPort = 18891;

        nPruneAfterHeight = 1000;

        genesis = CreateGenesisBlock(1532915420, 239712, 0x1e0ffff0, 1, 50 * COIN);
        consensus.hashGenesisBlock = genesis.GetHash();
        //assert(consensus.hashGenesisBlock == uint256S("0x00000a584fb9211f6dc67cebc024138caa9e387274bf91400cbb2aa49c53ceca"));
        //assert(genesis.hashMerkleRoot == uint256S("0x1b3952bab9df804c6f02372bb62df20fa2927030a4e80389ec14c1d86fc921e4"));
        assert(consensus.hashGenesisBlock == uint256S("00000a21e4f8644a97ae10074306546286dcc4e208094f99091dd3cb59203df3"));
        //00000e3091cd3a56c6b4790fb12d4b3388fdd26c335352c511610075217f5a17
        assert(genesis.hashMerkleRoot == uint256S("16c295a9a2ad0dd8453b4f9e7ba125e95b428f586856e0957c0e700781e8c469"));
        //bc4e20fc337c37d6ba4e07c955b1aaa2140c0ff574a331dd3c6fb2ed5f65ff12
        //assert(consensus.hashGenesisBlock == uint256S("0x00"));
        //assert(genesis.hashMerkleRoot == uint256S("0x00"));
        

        vFixedSeeds.clear(); //! Regtest mode doesn't have any fixed seeds.
        vSeeds.clear();  //! Regtest mode doesn't have any DNS seeds.

        fMiningRequiresPeers = false;
        fDefaultConsistencyChecks = true;
        fRequireStandard = false;
        fMineBlocksOnDemand = true;
        fTestnetToBeDeprecatedFieldRPC = false;

        nFulfilledRequestExpireTime = 5*60; // fulfilled requests expire in 5 minutes

        //00000e3091cd3a56c6b4790fb12d4b3388fdd26c335352c511610075217f5a17
        checkpointData = (CCheckpointData){
            boost::assign::map_list_of
            ( 0, uint256S("00000a21e4f8644a97ae10074306546286dcc4e208094f99091dd3cb59203df3")),
            0,
            0,
            0
        };
        // Regtest OpioidRX addresses start with 'r' (small r for regtest)
        base58Prefixes[PUBKEY_ADDRESS] = std::vector<unsigned char>(1,122);
        // Regtest OpioidRX script addresses start with '9'
        base58Prefixes[SCRIPT_ADDRESS] = std::vector<unsigned char>(1,20);
        // Regtest private keys start with '9' or 'c' (Bitcoin defaults) (?)
        base58Prefixes[SECRET_KEY] =     std::vector<unsigned char>(1,240);
        // Regtest OpioidRX BIP32 pubkeys start with 'tpub' (Bitcoin defaults)
        base58Prefixes[EXT_PUBLIC_KEY] = boost::assign::list_of(0x04)(0x35)(0x87)(0xCF).convert_to_container<std::vector<unsigned char> >();
        // Regtest OpioidRX BIP32 prvkeys start with 'tprv' (Bitcoin defaults)
        base58Prefixes[EXT_SECRET_KEY] = boost::assign::list_of(0x04)(0x35)(0x83)(0x94).convert_to_container<std::vector<unsigned char> >();

        // Regtest OpioidRX BIP44 coin type is '1' (All coin's testnet default)
        nExtCoinType = 1;
   }
};
static CRegTestParams regTestParams;

static CChainParams *pCurrentParams = 0;

const CChainParams &Params() {
    assert(pCurrentParams);
    return *pCurrentParams;
}

CChainParams& Params(const std::string& chain)
{
    if (chain == CBaseChainParams::MAIN)
            return mainParams;
    else if (chain == CBaseChainParams::TESTNET)
            return testNetParams;
    else if (chain == CBaseChainParams::REGTEST)
            return regTestParams;
    else
        throw std::runtime_error(strprintf("%s: Unknown chain %s.", __func__, chain));
}

void SelectParams(const std::string& network)
{
    SelectBaseParams(network);
    pCurrentParams = &Params(network);
}
